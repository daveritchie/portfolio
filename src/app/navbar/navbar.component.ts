import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  // show: boolean = false;
  toggle() {
    let x= document.getElementById("mynavbar");
    if(x.className === "mynavbar") {
      x.className += " responsive";
    }
    else {
      x.className = "mynavbar";
    }
    console.log(x.className);
  }
}
