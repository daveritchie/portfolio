import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  feedback = [
    {
      name: 'Likhith Nemani',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae enim itaque voluptatibus, quos doloribus perferendis iure! Vitae neque corporis expedita ratione quod adipisci praesentium! Illum amet veritatis labore quam'
    },
    {
      name: 'Likhith Nemani',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae enim itaque voluptatibus, quos doloribus perferendis iure! Vitae neque corporis expedita ratione quod adipisci praesentium! Illum amet veritatis labore quam'
    },
    {
      name: 'Likhith Nemani',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae enim itaque voluptatibus, quos doloribus perferendis iure! Vitae neque corporis expedita ratione quod adipisci praesentium! Illum amet veritatis labore quam'
    },
    {
      name: 'Likhith Nemani',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae enim itaque voluptatibus, quos doloribus perferendis iure! Vitae neque corporis expedita ratione quod adipisci praesentium! Illum amet veritatis labore quam'
    },
  ];
  description;
  name;
  temp = {
    name: '',
    desc:''
  };

  add() {
    this.temp.name = this.name;
    this.temp.desc = this.description;
    this.feedback.push(this.temp);
  }
  nullify() {
    this.name = '';
    this.description = '';
    this.temp.name = '';
    this.temp.desc = '';
  }
}
