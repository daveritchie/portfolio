import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  cards = [
    { name: 'Stella Maris School', year: '2015', grade: 9.7, image: '../../assets/stella.jpg' },
    { name: 'Ascent Junior College', year: '2015-2017', grade: 93.6, image: '../../assets/ascent.jpg' },
    { name: 'Gitam University', year: '2017-2021', grade: 8.19, image: '../../assets/gitam.jpg' }
  ]
}
