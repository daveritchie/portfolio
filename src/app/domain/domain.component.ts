import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-domain',
  templateUrl: './domain.component.html',
  styleUrls: ['./domain.component.css']
})
export class DomainComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  courses = [
    {name: 'C', skill: '75%'},
    {name: 'C++', skill: '80%'},
    {name: 'Java', skill: '70%'},
    {name: 'Python', skill: '55%'},
    {name: 'JavaScript', skill: '65%'},
    {name: 'HTML', skill: '80%'},
    {name: 'CSS', skill: '85%'},
    {name: 'Bootstrap', skill: '80%'},
    {name: 'Angular', skill: '70%'},
    {name: 'Ionic', skill: '60%'},
  ]
  

}
